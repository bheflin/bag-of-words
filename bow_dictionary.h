/*
 * bow_dictionary.h
 *
 *  Created on: Nov 29, 2014
 *      Author: root
 */

#ifndef BOW_DICTIONARY_H_
#define BOW_DICTIONARY_H_

#ifndef _EiC
#include <opencv2/opencv.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include <sstream>
#include <malloc.h>
#include <vector>
#endif

#include <cstdlib>

using namespace std;
using namespace cv;

//!!!!The SURF or SIFT descriptors are not free and therefore cannot be used in a commercial product!!!"

////////////////////////////
// EXTRACT KEYPOINTS
////////////////////////////
//cv::FeatureDetector * detector = new cv::DenseFeatureDetector(1, 8, 1.5, 2);
// cv::FeatureDetector * detector = new cv::FastFeatureDetector();
// cv::FeatureDetector * detector = new cv::GFTTDetector();
// cv::FeatureDetector * detector = new cv::MSER();
//cv::FeatureDetector * detector = new cv::ORB(5000);
//cv::FeatureDetector * detector = new cv::SIFT();
cv::FeatureDetector * detector = new cv::SURF(5000.0);
// cv::FeatureDetector * detector = new cv::BRISK(600.0);

////////////////////////////
// EXTRACT DESCRIPTORS
////////////////////////////
// cv::DescriptorExtractor * extractor = new cv::BriefDescriptorExtractor();
//cv::DescriptorExtractor * extractor = new cv::ORB();
//cv::DescriptorExtractor * extractor = new cv::SIFT();
cv::DescriptorExtractor * extractor = new cv::SURF(5000.0);
// cv::DescriptorExtractor * extractor = new cv::BRISK();
// cv::DescriptorExtractor * extractor = new cv::FREAK();

//Number of words in the dictionary
int dictionarySize = 1000;

//number of image per category (each category must contain the same number of images)
int imagesPerCategory = 25;

//Number of categories
int categories = 11;

//List that contains the path to each image category
string fileListName = "imageList.txt";

//define Term Criteria
TermCriteria tc(CV_TERMCRIT_ITER, 3, 0.001);

//retries number
int retries = 3;

//Kmeans flags
int flags = KMEANS_PP_CENTERS;

vector<string> imageList;


#endif /* BOW_DICTIONARY_H_ */
