//============================================================================
// Name        : bow_dictionary train.cpp
// Author      : RC Johnson, B. Heflin
// Version     : 1.0
// Copyright   : Securics
// Description : Program to train a BOW dictionary
//============================================================================
#ifdef _CH_
#pragma package <opencv>
#endif

#include "bow_test.h"

int main(int argc, char** argv) {

	//read in the training file
	string temp;
	ifstream infile(fileListNameTrain.c_str());
	while (!(infile.eof())) {
		infile >> temp;
		imageListTrain.push_back(temp);
	}

	//read in the testing file
	ifstream infileTest(fileListNameTest.c_str());
	while (!(infileTest.eof())) {
		infileTest >> temp;
		imageListTest.push_back(temp);
	}

	//mat to hold the training data
	cv::Mat trainingDataMat = cv::Mat::zeros(
			cvSize(dictionarySize, (imagesPerCategoryTrain * categoriesTrain)),
			CV_32FC1);

	//get the dictionary
	Mat dictionary;

	fs["vocabulary"] >> dictionary;

	fs.release();

	//create BoF (or BoW) descriptor extractor
	BOWImgDescriptorExtractor bowDE(extractor, matcher);

	//Set the dictionary with the vocabulary we created with bow_dictionary
	bowDE.setVocabulary(dictionary);

	//to store the input file names
	char filename[2048];

	int totalCount = 0;

	Mat labelsMat((imagesPerCategoryTrain * categoriesTrain), 1, CV_32FC1);

	//load the images and get features
	for (int outerLoop = 0; outerLoop < categoriesTrain; outerLoop++) {

		for (int loop = 0; loop < imagesPerCategoryTrain; loop++) {

			sprintf(filename, "%s%d.jpg", imageListTrain[outerLoop].c_str(),
					loop + 1);

			//read the image
			Mat img = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

			if (!img.data) {
				cout << "Training image data is empty" << endl;
				exit(-2);
			} else {
				//To store the keypoints that will be extracted by SIFT
				vector<KeyPoint> keypoints;

				//Detect SIFT keypoints (or feature points)
				detector->detect(img, keypoints);

				//To store the BoW (or BoF) representation of the image
				Mat bowDescriptor;

				//extract BoW (or BoF) descriptor from given image
				bowDE.compute(img, keypoints, bowDescriptor);

				//get the category label
				labelsMat.at<float>(totalCount, 0) = outerLoop + 1;

				//Write out the bowDescriptor in SVM training format.
				for (int i = 0; i < bowDescriptor.cols; i++) {

					trainingDataMat.at<float>(totalCount, i) = bowDescriptor.at<
							float>(0, i);
				}

				totalCount++;
			}
		}
	}

	//train the svm
	// Set up training data
	cout << "Going to train svm" << endl;

	// Train the SVM
	CvSVM SVM;

	//params will be different depending on SVM type chosen
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::RBF;
	params.term_crit.type = CV_TERMCRIT_ITER + CV_TERMCRIT_EPS;
	params.term_crit.max_iter = 10000;
	params.term_crit.epsilon = 0.98;
	params.gamma = 0.60;

	bool success = SVM.train_auto(trainingDataMat, labelsMat, Mat(), Mat(),
			params, 10);
	if (!success) {
		cout << "ERROR: Training failed!!" << endl;
		exit(-3);
	}

	else
		cout << "Training done" << endl;

	cv::Mat testingDataMat = cv::Mat::zeros(
			cvSize(dictionarySize, (imagesPerCategoryTest * categoriesTest)),
			CV_32FC1);

	//Step 2 - Obtain the BoF descriptor for given image/video frame.

	//create BoF (or BoW) descriptor extractor
	BOWImgDescriptorExtractor bowDE1(extractor, matcher);

	//Set the dictionary with the vocabulary we created in the first step
	bowDE1.setVocabulary(dictionary);

	totalCount = 0;

	for (int outerLoop = 0; outerLoop < categoriesTest; outerLoop++) {

		for (int loop = 0; loop < imagesPerCategoryTest; loop++) {

			sprintf(filename, "%s%d.jpg", imageListTest[outerLoop].c_str(),
					loop + 1);
			//read the image
			Mat img = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

			//To store the keypoints that will be extracted by SIFT
			vector<KeyPoint> keypoints;

			//Detect SIFT keypoints (or feature points)
			detector->detect(img, keypoints);

			//To store the BoW (or BoF) representation of the image
			Mat bowDescriptor;

			//extract BoW (or BoF) descriptor from given image
			bowDE.compute(img, keypoints, bowDescriptor);

			//Write out the bowDescriptor in SVM training format.
			for (int i = 0; i < bowDescriptor.cols; i++) {

				testingDataMat.at<float>(totalCount, i) =
						bowDescriptor.at<float>(0, i);

			}

			totalCount++;

		}	//end loop
	}	//end outerloop

	cv::Mat predicted(testingDataMat.rows, 1, CV_32F);

	//instantiate the output file
	ofstream outFile;
	outFile.open(outputFile.c_str());

	//sample coe to use the SVM on the testing data, output results to file and compute class accuracy
	int correctLabel;
	int correct = 0;
	int incorrect = 0;

	int correctBird = 0;
	int incorrectBird = 0;
	int correctDog = 0;
	int incorrectDog = 0;
	int correctkitFox = 0;
	int incorrectkitFox = 0;
	int incorrectRat = 0;
	int correctRat = 0;
	int incorrectSquirrel = 0;
	int correctSquirrel = 0;
	int correctBobcat = 0;
	int incorrectBobcat = 0;
	int correctCoyote = 0;
	int incorrectCoyote = 0;
	int correctGila = 0;
	int incorrectGila = 0;
	int incorrectRabbit = 0;
	int correctRabbit = 0;
	int incorrectSnake = 0;
	int correctSnake = 0;
	int incorrectTortoise = 0;
	int correctTortoise = 0;

	int ctr1 = 1;

	for (int i = 0; i < testingDataMat.rows; i++) {

		cv::Mat sample = testingDataMat.row(i);

		int predicted = SVM.predict(sample);
		cout << "Predicted[" << i << "] :" << predicted << endl;
		outFile << predicted;
		outFile << "\n";

		correctLabel = labelsMat.at<float>(i, 0);

		if ((int) predicted != correctLabel) {
			incorrect++;

			if (ctr1 <= 25) {
				incorrectBird++;
			} else if (ctr1 >= 26 && ctr1 <= 50) {
				incorrectDog++;
			} else if (ctr1 >= 51 && ctr1 <= 75) {
				incorrectkitFox++;
			} else if (ctr1 >= 76 && ctr1 <= 100) {
				incorrectRat++;
			} else if (ctr1 >= 101 && ctr1 <= 125) {
				incorrectSquirrel++;
			} else if (ctr1 >= 126 && ctr1 <= 150) {
				incorrectBobcat++;
			} else if (ctr1 >= 151 && ctr1 <= 175) {
				incorrectCoyote++;
			} else if (ctr1 >= 176 && ctr1 <= 200) {
				incorrectGila++;
			} else if (ctr1 >= 201 && ctr1 <= 225) {
				incorrectRabbit++;
			} else if (ctr1 >= 226 && ctr1 <= 250) {
				incorrectSnake++;
			} else {
				incorrectTortoise++;
			}

		}

		else {
			correct++;
			if (ctr1 <= 25) {
				correctBird++;
			} else if (ctr1 >= 26 && ctr1 <= 50) {
				correctDog++;
			} else if (ctr1 >= 51 && ctr1 <= 75) {
				correctkitFox++;
			} else if (ctr1 >= 76 && ctr1 <= 100) {
				correctRat++;
			} else if (ctr1 >= 101 && ctr1 <= 125) {
				correctSquirrel++;
			} else if (ctr1 >= 126 && ctr1 <= 150) {
				correctBobcat++;
			} else if (ctr1 >= 151 && ctr1 <= 175) {
				correctCoyote++;
			} else if (ctr1 >= 176 && ctr1 <= 200) {
				correctGila++;
			} else if (ctr1 >= 201 && ctr1 <= 225) {
				correctRabbit++;
			} else if (ctr1 >= 226 && ctr1 <= 250) {
				correctSnake++;
			} else {
				correctTortoise++;
			}

		}

		ctr1++;

	}
	outFile.close();

	cout << "Correct: " << correct << endl;
	cout << "Incorrect: " << incorrect << endl;

	float per = (float) correct / ((float) correct + (float) incorrect);
	cout << "Accuracy: " << per * 100 << endl;

	cout << "correct Bird: " << correctBird << " incorrect Bird: "
			<< incorrectBird << endl;
	cout << "correct Dog: " << correctDog << " incorrect Dog: " << incorrectDog
			<< endl;
	cout << "correct kitFox: " << correctkitFox << " incorrect kitFox: "
			<< incorrectkitFox << endl;
	cout << "correct rat: " << correctkitFox << " incorrect rat: "
			<< incorrectkitFox << endl;
	cout << "correct squirrel: " << correctSquirrel << " incorrect squirrel: "
			<< incorrectSquirrel << endl;
	cout << "correct bobcat: " << correctBobcat << " incorrect bobcat: "
			<< incorrectBobcat << endl;
	cout << "correct Coyote: " << correctCoyote << " incorrect Coyote: "
			<< incorrectCoyote << endl;
	cout << "correct Gila: " << correctGila << " incorrect Gila: "
			<< incorrectGila << endl;
	cout << "correct Rabbit: " << correctRabbit << " incorrect Rabbit: "
			<< incorrectRabbit << endl;
	cout << "correct Snake: " << correctSnake << " incorrect Snake: "
			<< incorrectSnake << endl;
	cout << "correct Tortoise: " << correctTortoise << " incorrect Tortoise: "
			<< incorrectTortoise << endl;

	exit(0);

}
