//============================================================================
// Name        : bow_dictionary train.cpp
// Author      : RC Johnson, B. Heflin
// Version     : 1.0
// Copyright   : Securics
// Description : Program to train a BOW dictionary
//============================================================================
#ifdef _CH_
#pragma package <opencv>
#endif

#include "bow_dictionary.h"

int main(int argc, char** argv) {

	string temp;
	ifstream infile(fileListName.c_str());
	while (!(infile.eof())) {
		infile >> temp;
		imageList.push_back(temp);
	}

	//Step 1 - Obtain the set of bags of features.
	cv::Mat trainingDataMat = cv::Mat::zeros(
			cvSize(dictionarySize, (imagesPerCategory * categories)), CV_32FC1);

	//to store the input file names
	char * filename = new char[100];

	//to store the current input image
	Mat input;

	//To store the keypoints that will be extracted by SIFT
	vector<KeyPoint> keypoints;

	//To store the descriptor of current image
	Mat descriptor;

	//To store all the descriptors that are extracted from all the images.
	Mat featuresUnclustered;

	cv::Mat objectDescriptors;

	//feature descriptors and build the vocabulary
	for (int outerLoop = 0; outerLoop < categories; outerLoop++) {
		for (int f = 0; f < imagesPerCategory; f += 1) {

			sprintf(filename, "%s%d.jpg", imageList[outerLoop].c_str(), f + 1);

			cout << "Loading: " << filename << endl;

			//open the file
			input = imread(filename, CV_LOAD_IMAGE_GRAYSCALE); //Load as grayscale

			//detect feature points
			detector->detect(input, keypoints);

			Mat tempDescriptor;

			//compute the descriptors for each keypoint
			extractor->compute(input, keypoints, objectDescriptors);

			//put the all feature descriptors in a single Mat object
			featuresUnclustered.push_back(objectDescriptors);
		}
	}
	cout << "training dictionary" << endl;

	//Create the BoW (or BoF) trainer
	BOWKMeansTrainer bowTrainer(dictionarySize, tc, retries, flags);

	//cluster the feature vectors
	Mat dictionary = bowTrainer.cluster(featuresUnclustered);

	//store the vocabulary
	FileStorage fs("dictionary.yml", FileStorage::WRITE);
	fs << "vocabulary" << dictionary;
	fs.release();

	cout << "Dictionary training complete" << endl;

}
